/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.elbit.discordpresence.utils.presence;

import net.arikia.dev.drpc.DiscordRichPresence;

/**
 *
 * @author test
 */
public class RichPresenceBuilder {
    
    RichPresence rpc = new RichPresence();
    
    public RichPresenceBuilder(String id) {
        this.rpc.setId(id);
    }
    
    @Deprecated
    public RichPresenceBuilder() {
        
    }

    @Deprecated
    public void id(String id) {
        this.rpc.setId(id);
    }

    public RichPresenceBuilder details(String details) {
        this.rpc.setDetails(details);
        return this;
    }

    public RichPresenceBuilder bigImageKey(String bigImageKey) {
        this.rpc.setBigImageKey(bigImageKey);
        return this;
    }

    public RichPresenceBuilder bigImageText(String bigImageText) {
        this.rpc.setBigImageText(bigImageText);
        return this;
    }

    public RichPresenceBuilder smallImageKey(String smallImageKey) {
        this.rpc.setSmallImageKey(smallImageKey);
        return this;
    }

    public RichPresenceBuilder smallimageText(String smallImageText) {
        this.rpc.setSmallImageText(smallImageText);
        return this;
    }

    public RichPresenceBuilder state(String partyText) {
        this.rpc.setState(partyText);
        return this;
    }

    public RichPresenceBuilder partySize(int partySize) {
        this.rpc.setPartySize(partySize);
        return this;
    }

    public RichPresenceBuilder partyMax(int partyMax) {
        this.rpc.setPartyMax(partyMax);
        return this;
    }

    public RichPresenceBuilder startTimeStamp(long startTimeStamp) {
        this.rpc.setStartTimeStamp(startTimeStamp);
        return this;
    }

    public RichPresenceBuilder endTimeStamp(long endTimeStamp) {
        this.rpc.setEndTimeStamp(endTimeStamp);
        return this;
    }
    
    public RichPresence build() {
        return this.rpc;
    }
    
}
