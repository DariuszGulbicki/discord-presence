/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.elbit.discordpresence.utils.presence;

import net.arikia.dev.drpc.DiscordRichPresence;

/**
 *
 * @author test
 */
public class RichPresence {
    
    private String id;
    private String details;
    private String bigImageKey;
    private String bigImageText;
    private String smallImageKey;
    private String smallimageText;
    private String partyText;
    private int partySize;
    private int partyMax;
    private long startTimeStamp;
    private long endTimeStamp;
    
    /*public RichPresence() {
        
        discord = new DiscordRichPresence.Builder();

    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getBigImageKey() {
        return bigImageKey;
    }

    public void setBigImageKey(String bigImageKey) {
        this.bigImageKey = bigImageKey;
    }

    public String getBigImageText() {
        return bigImageText;
    }

    public void setBigImageText(String bigImageText) {
        this.bigImageText = bigImageText;
    }

    public String getSmallImageKey() {
        return smallImageKey;
    }

    public void setSmallImageKey(String smallImageKey) {
        this.smallImageKey = smallImageKey;
    }

    public String getSmallImageText() {
        return smallimageText;
    }

    public void setSmallImageText(String smallimageText) {
        this.smallimageText = smallimageText;
    }

    public String getState() {
        return partyText;
    }

    public void setState(String partyText) {
        this.partyText = partyText;
    }

    public int getPartySize() {
        return partySize;
    }

    public void setPartySize(int partySize) {
        this.partySize = partySize;
    }

    public int getPartyMax() {
        return partyMax;
    }

    public void setPartyMax(int partyMax) {
        this.partyMax = partyMax;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public long getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(long endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }
    
}
