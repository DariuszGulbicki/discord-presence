/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.elbit.discordpresence.utils.presence;

import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;

/**
 *
 * @author test
 */
public class DiscordRichPresenceManager {

    private static DiscordRichPresence.Builder discord;
    private static boolean started = false;

    public static void send(RichPresence rpc) {
        if (DiscordRichPresenceManager.started) {
            DiscordRichPresenceManager.generateAndUpdateRichPresence(rpc);
        } else {
            if ("".equals(rpc.getId()) || rpc.getId() == null) {
                throw new IllegalArgumentException("Valid id is required to send Rich Presence");
            }
            DiscordRichPresenceManager.start(rpc.getId());
            DiscordRichPresenceManager.generateAndUpdateRichPresence(rpc);
            DiscordRichPresenceManager.started = true;
        }
    }

    public static void clear() {
        DiscordRPC.discordClearPresence();
        DiscordRichPresenceManager.endConnection();
        DiscordRichPresenceManager.started = false;
    }

    public static void endConnection() {
        DiscordRPC.discordShutdown();
        DiscordRichPresenceManager.started = false;
    }

    private static void start(String cID) {
        DiscordEventHandlers handlers = new DiscordEventHandlers.Builder().setReadyEventHandler((user) -> {
            DiscordRichPresence.Builder presence = new DiscordRichPresence.Builder("");
            presence.setDetails("");
            presence.setBigImage("", "");
            presence.setSmallImage("", "");
            DiscordRPC.discordUpdatePresence(presence.build());
        }).build();
        DiscordRPC.discordInitialize(cID, handlers, false);
        DiscordRPC.discordRegister(cID, "");
    }

    private static void generateAndUpdateRichPresence(RichPresence rpc) {
        discord = new DiscordRichPresence.Builder(rpc.getState());
        discord.setDetails(rpc.getDetails());
        discord.setBigImage(rpc.getBigImageKey(), rpc.getBigImageText());
        discord.setSmallImage(rpc.getSmallImageKey(), rpc.getSmallImageText());
        discord.setParty(rpc.getState(), rpc.getPartySize(), rpc.getPartyMax());
        discord.setStartTimestamps(rpc.getStartTimeStamp());
        discord.setEndTimestamp(rpc.getEndTimeStamp());
        DiscordRPC.discordUpdatePresence(discord.build());
    }

}
