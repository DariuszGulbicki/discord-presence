package tech.elbit.discordpresence.utils.settings;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 *
 * @author test
 */
public class SettingsManager {
    
    private static Settings currentSettings;
    
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
    
    private static final File SETTINGS_PATH = new File(System.getenv("APPDATA") + "/Elbit/Discord Presence/app.settings");

    public static Settings getCurrentSettings() {
        return currentSettings == null ? new Settings() : currentSettings;
    }

    public static void setCurrentSettings(Settings currentSettings) {
        SettingsManager.currentSettings = currentSettings;
    }
    
    public static void saveSettings() throws IOException {
        if (!SETTINGS_PATH.exists()) {
            SETTINGS_PATH.getParentFile().mkdirs();
            SETTINGS_PATH.createNewFile();
        }
        Files.writeString(SETTINGS_PATH.toPath(), gson.toJson(SettingsManager.getCurrentSettings()));
    }
    
    public static void loadSettings() throws IOException {
        if (!SETTINGS_PATH.exists()) {
            SETTINGS_PATH.getParentFile().mkdirs();
            SETTINGS_PATH.createNewFile();
            SettingsManager.setCurrentSettings(new Settings());
        } else {
            SettingsManager.setCurrentSettings(gson.fromJson(Files.readString(SETTINGS_PATH.toPath()), Settings.class));
        }
    }
    
}
