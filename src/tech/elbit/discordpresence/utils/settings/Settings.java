/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.elbit.discordpresence.utils.settings;

import java.io.File;

/**
 *
 * @author test
 */
public class Settings {
    
    boolean wasLastPresenceSaved = false;
    String defaultSaveLocation = System.getenv("HOMEPATH") + "/Documents/DiscordPresence";
    String lastPresenceSaveLocation = System.getenv("APPDATA") + "/Elbit/DiscordPresence/saved_presences/last.rpc";

    public boolean wasLastPresenceSaved() {
        return wasLastPresenceSaved;
    }

    public void setLastPresenceSaved(boolean wasLastPresenceSaved) {
        this.wasLastPresenceSaved = wasLastPresenceSaved;
    }
    
    public void setDefaultSaveLocation(String defaultSaveLocation) {
        this.defaultSaveLocation = defaultSaveLocation;
    }
    
    public void setLastPresenceSaveLocation(String lastPresenceSaveLocation) {
        this.lastPresenceSaveLocation = lastPresenceSaveLocation;
    }

    public String getDefaultSaveLocation() {
        return defaultSaveLocation;
    }

    public String getLastPresenceSaveLocation() {
        return lastPresenceSaveLocation;
    }
    
}
