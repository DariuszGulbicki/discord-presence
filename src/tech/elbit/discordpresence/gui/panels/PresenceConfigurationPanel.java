package tech.elbit.discordpresence.gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import tech.elbit.discordpresence.utils.listeners.MouseListenerAdapter;
import tech.elbit.discordpresence.utils.presence.DiscordRichPresenceManager;
import tech.elbit.discordpresence.utils.presence.RichPresence;
import tech.elbit.discordpresence.utils.presence.RichPresenceBuilder;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class PresenceConfigurationPanel extends JPanel {

    private JTextField idField = new JTextField("id");
    private JTextField detailsField = new JTextField("details");
    private JTextField bigImageKeyField = new JTextField("bigImageKey");
    private JTextField bigImageTextField = new JTextField("bigImageText");
    private JTextField smallImageKeyField = new JTextField("smallImageKey");
    private JTextField smallImageTextField = new JTextField("smallImageText");
    private JTextField partyTextField = new JTextField("partyText");
    private JTextField partySizeField = new JTextField("partySize");
    private JTextField partyMaxField = new JTextField("partyMax");
    
    private JLabel textLabel = new JLabel("TEXT", JLabel.CENTER);
    private JLabel graphicsLabel = new JLabel("GRAPHICS", JLabel.CENTER);
    private JLabel partyLabel = new JLabel("PARTY", JLabel.CENTER);
    
    private JButton timeButton = new JButton("Set time options");
    
    private JButton sendUpdateButton = new JButton("SEND");
    private JButton clearButton = new JButton("CLEAR");
    
    private boolean firstSend = true;
    
    public PresenceConfigurationPanel() {
        initPanel();
        initComponents();
        addComponents();
    }
    
    private void initPanel() {
        this.setLayout(new GridBagLayout());
    }
    
    private void initComponents() {
        clearButton.setEnabled(false);
        clearButton.addMouseListener(new MouseListenerAdapter() {
        
            @Override
            public void mousePressed(MouseEvent e) {
                DiscordRichPresenceManager.clear();
                sendUpdateButton.setText("SEND");
                clearButton.setEnabled(false);
            }
            
        });
        sendUpdateButton.addMouseListener(new MouseListenerAdapter() {
        
            @Override
            public void mousePressed(MouseEvent e) {
                DiscordRichPresenceManager.send(getPresence());
                sendUpdateButton.setText("UPDATE");
                clearButton.setEnabled(true);
            }
            
        });
    }
    
    private void addComponents() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.gridwidth = 2;
        c.insets = new Insets(10, 10, 10, 10);
        this.add(idField, c);
        c.gridy = 1;
        this.add(textLabel, c);
        c.gridy = 2;
        c.gridwidth = 1;
        this.add(detailsField, c);
        c.gridx = 1;
        this.add(partyTextField, c);
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 2;
        this.add(graphicsLabel, c);
        c.gridy = 4;
        c.gridwidth = 1;
        this.add(bigImageKeyField, c);
        c.gridx = 1;
        this.add(bigImageTextField, c);
        c.gridx = 0;
        c.gridy = 5;
        this.add(smallImageKeyField, c);
        c.gridx = 1;
        this.add(smallImageTextField, c);
        c.gridx = 0;
        c.gridy = 6;
        c.gridwidth = 2;
        this.add(partyLabel, c);
        c.gridy = 7;
        c.gridwidth = 1;
        this.add(partySizeField, c);
        c.gridx = 1;
        this.add(partyMaxField, c);
        c.gridx = 0;
        c.gridy = 8;
        c.gridwidth = 2;
        this.add(timeButton, c);
        c.gridy = 9;
        c.gridwidth = 1;
        this.add(clearButton, c);
        c.gridx = 1;
        this.add(sendUpdateButton, c);
    }
    
    public RichPresence getPresence() {
        return new RichPresenceBuilder(idField.getText())
                .details(detailsField.getText())
                .state(partyTextField.getText())
                .bigImageKey(bigImageKeyField.getText())
                .bigImageText(bigImageTextField.getText())
                .smallImageKey(smallImageKeyField.getText())
                .smallimageText(smallImageTextField.getText())
                .partySize(Integer.parseInt(partySizeField.getText()))
                .partyMax(Integer.parseInt(partyMaxField.getText()))
                .build();
    }
    
    public void setFields(RichPresence presence) {
        idField.setText(presence.getId());
        detailsField.setText(presence.getDetails());
        partyTextField.setText(presence.getState());
        bigImageKeyField.setText(presence.getBigImageKey());
        bigImageTextField.setText(presence.getBigImageText());
        smallImageKeyField.setText(presence.getSmallImageKey());
        smallImageTextField.setText(presence.getSmallImageText());
        partySizeField.setText(Integer.toString(presence.getPartySize()));
        partyMaxField.setText(Integer.toString(presence.getPartyMax()));
    }
    
}
