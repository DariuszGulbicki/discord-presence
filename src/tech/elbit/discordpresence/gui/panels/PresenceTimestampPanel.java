package tech.elbit.discordpresence.gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class PresenceTimestampPanel extends JPanel {
    
    private JPanel timeButtonsPanel = new JPanel();

    private JRadioButton noTimeButton = new JRadioButton("none");
    private JRadioButton timeElapsedButton = new JRadioButton("time elapsed");
    private JRadioButton timeLeftButton = new JRadioButton("time left");
    
    public PresenceTimestampPanel() {
        initPanel();
        initComponents();
        addComponents();
    }
    
    private void initPanel() {
        this.setLayout(new GridBagLayout());
    }
    
    private void initComponents() {
        initTimeButtonsPanel();
    }
    
    private void addComponents() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.insets = new Insets(5, 5, 5, 5);
        this.add(timeButtonsPanel, c);
        
    }
    
    private void initTimeButtonsPanel() {
        initTimeButtons();
        timeButtonsPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.insets = new Insets(10, 10, 10, 10);
        timeButtonsPanel.add(noTimeButton, c);
        c.gridx = 1;
        timeButtonsPanel.add(timeElapsedButton, c);
        c.gridx = 2;
        timeButtonsPanel.add(timeLeftButton, c);
    }
    
    private void initTimeButtons() {
        ButtonGroup bg = new ButtonGroup();
        bg.add(noTimeButton);
        bg.add(timeElapsedButton);
        bg.add(timeLeftButton);
    }
    
}
