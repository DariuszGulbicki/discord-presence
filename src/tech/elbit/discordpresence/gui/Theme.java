package tech.elbit.discordpresence.gui;

import java.awt.Color;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public enum Theme {

    DARK_BACKGROUND(new Color(26, 26, 26)),
    BACKGROUND(new Color(46, 46, 46)),
    LIGHT(new Color(235, 215, 109)),
    MAIN(new Color(255, 178, 0)),
    TEXT(new Color(255, 255, 255)),
    DARK_TEXT(new Color(150, 150, 150));
    
    private Color color;

    Theme(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }
    
}
