package tech.elbit.discordpresence.gui.proporties;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class ComponentConfigurations {

    private TextFieldConfiguration textFieldConfiguration = new TextFieldConfiguration();

    public TextFieldConfiguration getTextFieldConfiguration() {
        return textFieldConfiguration;
    }

    public void setTextFieldConfiguration(TextFieldConfiguration textFieldConfiguration) {
        this.textFieldConfiguration = textFieldConfiguration;
    }
    
    public void fromFile(String path) {
        
    }
    
    public void toFile(String path) {
        
    }
    
}
