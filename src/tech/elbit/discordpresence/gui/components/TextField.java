package tech.elbit.discordpresence.gui.components;

import javax.swing.JTextField;
import tech.elbit.discordpresence.gui.proporties.ComponentConfigurations;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class TextField extends JTextField{
    
    private String placeholder;
    private boolean displayPlaceholder = false;

    public TextField() {
    
    }
    
    public TextField(ComponentConfigurations config) {
    
    }
    
    public TextField(String text) {
        setText(text);
    }
    
    public TextField(String text, ComponentConfigurations config) {
        setText(text);
    }
    
    public TextField(String text, int horizontalAlignment) {
        this.setText(text);
        this.setHorizontalAlignment(horizontalAlignment);
    }
    
    public TextField(String text, int horizontalAlignment, ComponentConfigurations colors) {
        this.setText(text);
        this.setHorizontalAlignment(horizontalAlignment);
    }
    
    public void setPlaceholder(String placeholder) {
        
    }
    
    public String getPlaceholder() {
        return this.placeholder;
    }
    
    private void styleElement(ComponentConfigurations config) {
        
    }
    
}
