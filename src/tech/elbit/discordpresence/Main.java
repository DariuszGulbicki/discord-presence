package tech.elbit.discordpresence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import tech.elbit.discordpresence.gui.panels.PresenceConfigurationPanel;
import tech.elbit.discordpresence.utils.presence.DiscordRichPresenceManager;
import tech.elbit.discordpresence.utils.presence.RichPresence;
import tech.elbit.discordpresence.utils.settings.Settings;
import tech.elbit.discordpresence.utils.settings.SettingsManager;

/**
 *
 * @author Dariusz Gulbicki
 * @author Elbit
 * @author https://elbit.tech
 * @author kontakt@dariuszgulbicki.pl
 * @author contact@elbit.tech
 * @version 0.1
 * @since 0.1
 */
public class Main {
    
    static Settings settings;
    
    public static void main(String[] args) {
        try {
            SettingsManager.loadSettings();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        settings = SettingsManager.getCurrentSettings();
        PresenceConfigurationPanel configPanel = new PresenceConfigurationPanel();
        loadLastPresence(configPanel);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            DiscordRichPresenceManager.endConnection();
            saveLastPresence(configPanel);
        }));
        JFrame frame = new JFrame();
        frame.setTitle("Discord Presence by Elbit");
        frame.setMinimumSize(new Dimension(500, 700));
        frame.add(configPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    
    private static void loadLastPresence(PresenceConfigurationPanel panel) {
        File save = new File(settings.getLastPresenceSaveLocation());
        if (save.exists()) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            try {
                panel.setFields(gson.fromJson(Files.readString(save.toPath()), RichPresence.class));
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static void saveLastPresence(PresenceConfigurationPanel panel) {
        File save = new File(settings.getLastPresenceSaveLocation());
        if (!save.exists()) {
            save.getParentFile().mkdirs();
            try {
                save.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            Files.writeString(save.toPath(), gson.toJson(panel.getPresence()), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
