[![pipeline status](https://gitlab.com/DariuszGulbicki/discord-presence/badges/master/pipeline.svg)](https://gitlab.com/DariuszGulbicki/discord-presence/-/commits/main)
[![coverage report](https://gitlab.com/DariuszGulbicki/discord-presence/badges/master/coverage.svg)](https://gitlab.com/DariuszGulbicki/discord-presence/-/commits/main)

# ![Project logo](https://gitlab.com/DariuszGulbicki/discord-presence/-/raw/master/logo/logo_scaled.png?inline=false) Discord Presence

Discord presence is a standalone program used for displaying Rich Presence ("Now playing") messages on discord.  

## Installation

[Latest Version](https://gitlab.com/api/v4/projects/30912680/jobs/artifacts/master/download?job=build)

## Usage

1. Go to [**Discord developer portal**](https://discord.com/developers/applications)
2. Click on **New Application** to register a new application
3. Whatever you enter in the **Name** field will be displayed as "game" in your message (e.g. if you enter *testApp* then *"Now playing testApp"* will be displayed)
4. Navigate to the **Rich Presence** section and choose **Art Assets** then use **Add Image(s)** button to upload assets that will be displayed inside your Rich Presence message *<u>(optional)</u>*
5. Use **Visualizer** under **Rich Presence** section to preview your Rich Presence design *<u>(optional)</u>*
6. Run *DiscordPresence* and **copy the application id from Discord Developer to the id field in DiscordPresence**
7. **Fill in the other fields** inside the *DiscordPresence* gui
8. Click on **SEND**
9. Your new Rich Presence should be visible soon

> ### Note
>
> Discord desktop app **MUST** be installed for this app to work properly

## Building from source

To build project from source you will need:
* JDK 11 (For example coretto 11)
* Apache ANT

Start by using "cd" command (or equivalent) to navigate into the directory containing project files. Then build the project using ant as in example shown below:

```bash
ant -buildfile build.xml -Dnb.internal.action.name=rebuild clean jar
```

After building the project you can simply run it.

> ### Note
> When Discord Presence is compatible with Java 8, it **MUST** be built using JDK 11. Building using any older version of JDK **WILL** cause errors.

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)

> ### Note
> The images in this repository are **NOT** distributed under the [MIT](https://choosealicense.com/licenses/mit/) license. To use the images (project logo for example) you **MUST** obtain a license from Project author or Image author. 
